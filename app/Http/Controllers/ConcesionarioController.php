<?php

namespace App\Http\Controllers;

use App\Concesionario;
use App\Carro;
use Illuminate\Http\Request;

class ConcesionarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $concesionarios = Concesionario::with('Carros')->get();        
        return view('concesionario.index', compact('concesionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('concesionario.create');
    }

    public function store(Request $request)
    {
        $concesionario = Concesionario::create([
            'nombre' => $request->nombre,
        ]);

        return redirect()->route('concesionarios.index')->with('success', 'Se registró el concesionario de forma exitosa');
    }

    public function show(Concesionario $concesionario)
    {
        $listaCarros = Carro::where("concesionario_id", $concesionario->id)->get();
        return view('concesionario.show', compact('concesionario','listaCarros'));
    }


    public function edit(Concesionario $concesionario)
    {
        return view('concesionario.edit', compact('concesionario'));
    }

    public function update(Request $request, Concesionario $concesionario)
    {
        $concesionario->update($request->all());
        return redirect()->route('concesionarios.index')->with('success', 'Se actualizó el concesionario de forma exitosa');
    }

 
    public function destroy(Concesionario $item)
    {
        dd($item);
        
        //$item->delete();
        //return redirect()->back()->with('success', 'Se eliminó el concesionario de forma exitosa');
    }
}

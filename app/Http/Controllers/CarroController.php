<?php

namespace App\Http\Controllers;

use App\Carro;
use App\Concesionario;
use Illuminate\Http\Request;

class CarroController extends Controller
{
   
    public function index()
    {
        
        $listaCarros =  Carro::with('Concesionario')->get();
        return view('carros.index', compact('listaCarros')); // array asociativo [listaCarros => $listaCarros]
    }

    
    public function create()
    {
        $listaConcecionarios = Concesionario::all();
      
        return view('carros.create',compact('listaConcecionarios'));
    }

    
    public function store(Request $request)
    {

        //$carro = Carro::create($request->all());
    
        $carro = Carro::create([
            'nombre' => $request->nombre,
            'placa' => $request->placa,
            'modelo' => $request->modelo,
            'concesionario_id' => $request->concesionario_id
        ]);

        return redirect()->route('carros.index')->with('success', 'Se registró el carro de forma exitosa');
    }

    public function show(Carro $carro)
    {
        return view('carros.show', compact('carro'));
    }

    
    public function edit(Carro $carro)
    {
        $listaConcecionarios = Concesionario::all();
        return view('carros.edit', compact('carro', 'listaConcecionarios'));
    }

   
    public function update(Request $request, Carro $carro)
    {
        $carro->update($request->all());
        return redirect()->route('carros.index')->with('success', 'Se actualizó el carro de forma exitosa');
    }

   
    public function destroy(Carro $carro)
    {
        $carro->delete();
        return redirect()->back()->with('success', 'Se eliminó el carro de forma exitosa');
    }
}

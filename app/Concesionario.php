<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Concesionario extends Model
{
    protected $fillable = [

        'nombre'
        
    ];

    // Muteadores

        // Getters

        // Setters
        
    // Relaciones

    public function Carros()
    {
        return $this->hasMany(Carro::class);
    }

    // Metodos
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carro extends Model
{

    // Atributos

    protected $fillable = [
        'nombre',
        'placa',
        'modelo',
        'concesionario_id'
    ];

    // protected $guard = [
    //     ''
    // ];

    // Muteadores

        // Getters

        // Setters
        
    // Relaciones

    public function Concesionario()
    {
        return $this->belongsTo(Concesionario::class);
    }

    // Metodos
}

<?php

use Faker\Generator as Faker;

$factory->define(App\Concesionario::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'carro_id' => $faker->unique()->numberBetween($min = 1, $max = 500)
    ];
});

<?php

use Faker\Generator as Faker;

$factory->define(App\Carro::class, function (Faker $faker) {
    return [
        'nombre' => $faker->name,
        'placa' => str_random(6),
        'modelo' => $faker->numberBetween($min = 2000, $max = 2020),
    ];
});

@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body text-center">
                        <h3>¡Concesionario <b>{{$concesionario->nombre}}</b>!</h3>
                        <hr>
                        <div class="text-left">
                            <ul>
                                @if (count($listaCarros) <= 0)
                                    <p class="text-center">No hay carros en este concesionario.</p>
                                @else
                                    @foreach ($listaCarros as $item)
                                        <li>{{$item->nombre}}</li>
                                    @endforeach
                                @endif
                                
                            </ul>
                        </div>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-block btn-success" href="{{route('concesionarios.index')}}">Volver.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
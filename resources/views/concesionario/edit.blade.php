@extends('layouts.app')

@section('contenido')
<div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <form action="{{route('concesionarios.update', $concesionario)}}" method="POST" class="form-group">

                        @csrf  @method("PUT")
                        <label for="nombre">Nombre</label>
                        <input type="text"  class="form-control" value="{{$concesionario->nombre}}" id="nombre" name="nombre">
                        <input type="submit" value="Guardar" class="mt-4 btn btn-block btn-success">
                </form>
            </div>
        </div>
</div>
@endsection
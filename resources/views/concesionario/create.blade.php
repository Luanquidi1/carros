@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <h1 class="text-center">¡Registra tu Concesionario!</h1>
                <form action="{{route('concesionarios.store')}}" method="POST" class="form-group">

                        @csrf
                        <label for="nombre">Nombre</label>
                        <input type="text"  class="form-control" value="" id="nombre" name="nombre">

                        <input type="submit" value="Registrar" class="mt-4 btn btn-block btn-primary">
                </form>
                <a class="d-flex justify-content-center" href="{{route('concesionarios.index')}}">Volver</a>
            </div>
        </div>
    </div>
@endsection
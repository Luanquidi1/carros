@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="text-center">Listado de Concesionarios</h1>
            <a class="float-right btn btn-primary my-2" href="{{route('concesionarios.create')}}"><i class="fas fa-plus"></i></a>
                
                    @if (Session::has('success'))
                        <div id="mensaje">
                            <div class="alert alert-success text-center">{{Session::get('success')}}</div>
                        </div>
                    @endif
                
                <table class="table table-striped">
                    <thead class="thead-dark text-center">
                        <tr>
                            <th width="15px">Id</th>
                            <th width="250px">Nombre</th>
                            <th width="200px">Acciones</th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($concesionarios as $item)
                        <tr>
                            <th>{{$item->id}}</th>
                            <td>{{$item->nombre}}</td>
                            <td>
                                <a href="{{route('concesionarios.show', $item)}}" class="btn btn-info"><i class="far fa-eye"></i></a>
                                <a href="{{route('concesionarios.edit', $item)}}" class="btn btn-success"><i class="far fa-edit"></i></a> 
                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="far fa-trash-alt"></i></a>
                                @include('concesionario.deleteModal')                                
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('mis-scripts')
    <script src="{{ asset('js/myApp.js')}}"></script>
@endpush
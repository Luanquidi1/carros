@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <h1 class="text-center">¡Registra tu Carro!</h1>
                <form action="{{route('carros.store')}}" method="POST" class="form-group">

                        @csrf
                        <label for="nombre">Nombre</label>
                        <input type="text"  class="form-control" value="" id="nombre" name="nombre">
                        <label for="placa">Placa</label>
                        <input type="text"  class="form-control" value="" id="placa" name="placa">
                        <label for="modelo">Modelo</label>
                        <input type="number"  min="2000" max="2020" class="form-control" value="" id="modelo" name="modelo">
                        <label for="concesionario_id">Concesionario</label>
                        <select class="form-control" name="concesionario_id" id="concesionario_id">
                            @foreach ($listaConcecionarios as $item)
                                <option value="{{$item->id}}">{{$item->nombre}}</option>
                            @endforeach
                        </select>
                        <input type="submit" value="Registrar" class="mt-4 btn btn-block btn-primary">
                </form>
                <a class="d-flex justify-content-center" href="{{route('carros.index')}}">Volver</a>
            </div>
        </div>
    </div>
@endsection
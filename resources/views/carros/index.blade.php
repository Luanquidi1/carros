@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
             
                <h1 class="text-center">Listado de Carros</h1>
                
                @auth
                <a class="float-right btn btn-primary my-2" href="{{route('carros.create')}}"><i class="fas fa-plus"></i></a>
                @endauth
                
                    @if (Session::has('success'))
                        <div id="mensaje">
                            <div class="alert alert-success text-center">{{Session::get('success')}}</div>
                        </div>
                    @endif
                
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th width="15px">Id</th>
                            <th>Nombre</th>
                            <th>Placa</th>
                            <th>Modelo</th>
                            <th>Concesionario</th>                       
                            <th width="200px">Acciones</th>                                                        
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listaCarros as $carro)
                        <tr>
                            <th>{{$carro->id}}</th>
                            <td>{{$carro->nombre}}</td>
                            <td>{{$carro->placa}}</td>
                            <td>{{$carro->modelo}}</td>
                            <td>
                                {{$carro->Concesionario->nombre}}
                            </td>
                            <td>
                                <a href="{{route('carros.show', $carro)}}" class="btn btn-info"><i class="far fa-eye"></i></a>
                                @auth
                                    <a href="{{route('carros.edit', $carro)}}" class="btn btn-success"><i class="far fa-edit"></i></a>
                                    <a href="{{route('carros.destroy', $carro)}}" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"><i class="far fa-trash-alt"></i></a>
                                    @include('carros.deleteModal')  
                                @endauth
                                                             
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@push('mis-scripts')
    <script src="{{ asset('js/myApp.js') }}"></script>
@endpush
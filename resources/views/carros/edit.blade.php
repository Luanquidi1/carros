@extends('layouts.app')

@section('contenido')
<div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <form action="{{route('carros.update', $carro)}}" method="POST" class="form-group">

                        @csrf  @method("PUT")
                        <label for="nombre">Nombre</label>
                        <input type="text"  class="form-control" value="{{$carro->nombre}}" id="nombre" name="nombre">
                        <label for="placa">Placa</label>
                        <input type="text"  class="form-control" value="{{$carro->placa}}" id="placa" name="placa">
                        <label for="modelo">Modelo</label>
                        <input type="number"  min="2000" max="2020" class="form-control" value="{{$carro->modelo}}" id="modelo" name="modelo">
                        <label for="concesionario_id">Concesionario</label>
                        <select class="form-control" value="{{$carro->concesionario_id}}" name="concesionario_id" id="concesionario_id">
                            @foreach ($listaConcecionarios as $item)
                                <option value="{{$item->id}}">{{$item->nombre}}</option>
                            @endforeach
                        </select>
                        

                        <input type="submit" value="Guardar" class="mt-4 btn btn-block btn-success">
                </form>
            </div>
        </div>
</div>
@endsection
@extends('layouts.app')

@section('contenido')
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body text-center">
                        <p><b>Nombre:</b> {{$carro->nombre}}</p>
                        <p><b>Placa:</b> {{$carro->placa}}</p>
                        <p><b>Modelo:</b> {{$carro->modelo}}</p>
                        <p><b>Concesionario:</b> {{$carro->Concesionario->nombre}}</p>
                    </div>
                    <div class="card-footer">
                        <a class="btn btn-block btn-success" href="{{route('carros.index')}}">Volver.</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection